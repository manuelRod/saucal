<?php
/*
 * Plugin Name: Saucal Test project
 * Description: My-account extension
 * Version: 1.0.0
 * Author: manuel.rodriguez.rosado@gmail.com
 * Author URI: https://www.linkedin.com/in/manuelengineer/
 * Text Domain: saucal-test
 * Domain Path: /languages
 */

// If this file is accessed, then abort.
if (!defined('WPINC')) {
    die;
}

/**
 * On Activation:
 *  - Check for woo, otherwise do not activate
 *  - Add new api endpoint and flush the rules
 */
register_activation_hook(__FILE__, function () {
    if (!in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
        wp_die('Sorry, but this plugin requires woocommerce to be activated. <br><a href="' . admin_url('plugins.php') . '">&laquo; Return to Plugins</a>');
    }

    add_rewrite_endpoint( 'api', EP_ROOT | EP_PAGES );
    flush_rewrite_rules();
});


// Register autoloader
spl_autoload_register( 'saucal_test_project_autoloader' );

function saucal_test_project_autoloader( $class_name ) {

    if ( is_numeric(strpos( $class_name, 'Saucal' ) ) ) {

        $classes_dir = realpath( plugin_dir_path( __FILE__ ) ) . DIRECTORY_SEPARATOR;
        $class_file  = str_replace( '\\', DIRECTORY_SEPARATOR, $class_name ) . '.php';
        // Delete main namespace from class name
        $class_file  = str_replace('Saucal' . DIRECTORY_SEPARATOR, '', $class_file);
        require_once $classes_dir . $class_file;
    }
}

define ('SAUCAL_API_SETTINGS_KEY', '_saucal_api_settings');

new \Saucal\Inc\Inc();



