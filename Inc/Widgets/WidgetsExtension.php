<?php
/**
 * Class to hook all widgets.
 *
 * User: manu
 * Date: 11/06/2018
 * Time: 12:48
 */
namespace Saucal\Inc\Widgets;

/**
 * Class WidgetsExtension
 * @package Saucal\Inc\Widgets
 */
class WidgetsExtension {

    function hookToWordpress() {
        add_action( 'widgets_init', [$this, 'registerSaucalWidget'] );
    }

    function registerSaucalWidget() {
        register_widget( 'Saucal\Inc\Widgets\SaucalWidget' );
    }
}
