<?php
/**
 * Saucal Widget Template
 *
 * User: manu
 * Date: 11/06/2018
 * Time: 15:28
 */

defined( 'ABSPATH' ) || exit;

/**
 * Hook: saucal_before_widget.
 */
do_action( 'saucal_before_widget' );
$cacheLayer = new \Saucal\Inc\ApiDriver\CacheDriver(get_current_user_id());
$information = $cacheLayer->fetchInformation();
?>
    <?php if (!is_array($information)) : ?>
        <?php _e('Not information available', 'saucal-test'); ?>
    <?php else: ?>
        <ul>
        <?php foreach ($information as $key => $value): ?>
            <li>
                <?php esc_attr_e( $key, 'saucal-test' ); ?> : <?php esc_attr_e( $value, 'saucal-test' ); ?>
            </li>
        <?php endforeach;?>
        </ul>
    <?php endif; ?>

<?
/**
 * Hook: saucal_after_widget.
 */
do_action( 'saucal_after_widget' );