<?php
/**
 * Class to initiate all hooks
 *
 * User: manu
 * Date: 11/06/2018
 * Time: 12:45
 */
namespace Saucal\Inc;
use Saucal\Inc\WooExtension\WooExtension;
use Saucal\Inc\Widgets\WidgetsExtension;

/**
 * Class Inc
 * @package Saucal\Inc
 */
class Inc {

    function __construct() {
        (new WooExtension())->hookToWordPress();
        (new WidgetsExtension())->hookToWordPress();
    }
}

