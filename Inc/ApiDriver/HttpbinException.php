<?php
/**
 * HttpbinException Exception
 * User: manu
 * Date: 11/06/2018
 * Time: 13:04
 */

namespace Saucal\Inc\ApiDriver;
use Throwable;

/**
 * Class HttpbinException
 * @package Saucal\Inc\ApiDriver\HttpbinException
 */
class HttpbinException extends ApiExceptionAbstract {

    function __construct($message = "", $code = 0, Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}


