<?php
/**
 * Abstract Exception
 * User: manu
 * Date: 11/06/2018
 * Time: 13:04
 */

namespace Saucal\Inc\ApiDriver;

/**
 * Class ApiExceptionAbstract
 * @package Saucal\Inc\ApiDriver\ApiExceptionAbstract
 */
abstract class ApiExceptionAbstract extends \Exception {

}


