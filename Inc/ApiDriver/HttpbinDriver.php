<?php
/**
 * Httpbin Driver
 * User: manu
 * Date: 11/06/2018
 * Time: 13:04
 */


namespace Saucal\Inc\ApiDriver;

/**
 * Class HttpbinDriver
 * @package Saucal\Inc\ApiDriver\HttpbinDriver
 */
class HttpbinDriver extends ApiDriverAbstract {

    /**
     * Httpbin endpont
     */
    const ENDPOINT = 'https://httpbin.org/';

    function __construct() {
        $this->authenticate();
    }

    /**
     * In this case, there is no authentication required
     *
     * @return bool
     */
    function authenticate() {
        return true;
    }

    /**
     * @param array $arguments Alphanumeric list of elements.
     * @return array Headers returned there as the return data from the API.
     * @throws HttpbinException
     */
    function fetch(array $arguments) {

        $response = wp_remote_post( self::ENDPOINT . 'post', [
                'method' => 'POST',
                'httpversion' => '1.0',
                'blocking' => true,
                'headers' => [],
                'body' => $arguments,
                'cookies' => []
            ]
        );

        if (is_wp_error($response) || $response['response']['code'] != 200) {
            throw new HttpbinException($response['response']['message']);
        }

        return current((array)$response['headers']);
    }

}

