<?php
/**
 * Abstract Driver to be implemented by all drivers (if there are more sources)
 * User: manu
 * Date: 11/06/2018
 * Time: 13:04
 */

namespace Saucal\Inc\ApiDriver;

/**
 * Class ApiDriverAbstract
 * @package Saucal\Inc\ApiDriver\ApiDriverAbstract
 */
abstract class ApiDriverAbstract {

    /**
     * @return bool
     */
    abstract function authenticate();

    /**
     * @param array $arguments
     * @return array
     */
    abstract function fetch(array $arguments);

}

