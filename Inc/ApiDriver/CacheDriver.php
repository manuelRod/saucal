<?php
/**
 * Cache layer to retrieve information
 *
 * User: manu
 * Date: 11/06/2018
 * Time: 17:23
 */

namespace Saucal\Inc\ApiDriver;

/**
 * Class CacheDriver
 * @package Saucal\Inc\ApiDriver
 */
class CacheDriver {

    /**
     * Cache times out every 30m
     */
    const TIMEOUT = 1800;

    /**
     * Holds cached information
     */
    const CACHED_INFO_KEY           = '_saucal_cached_info_key';

    /**
     * Holds settings used to cache information
     */
    const CACHED_INFO_SETTINGS_KEY  = '_saucal_cached_info_settings_key';

    /**
     * Holds timestamp of cached info
     */
    const CACHED_TIMESTAMP_KEY      = '_saucal_cached_timestamp_key';

    /**
     * @var int $userId
     */
    protected $userId;

    /**
     * @var string
     */
    protected $currentSettings;

    /**
     * CacheDriver constructor.
     *
     * @param int $userId
     */
    function __construct($userId) {
        $this->userId = $userId;
        $this->currentSettings = $this->getUserMeta(SAUCAL_API_SETTINGS_KEY);
    }

    /**
     * If required, gets info from the api and updates it accordingly.
     * In case of exception, retrieves cached information.
     * If no need for new api query, uses its cache version.
     *
     * @return mixed
     */
    public function fetchInformation() {
        if ($this->isTimedOut() || $this->isDifferentSettings()) {
            try {
                $newData = (new HttpbinDriver())->fetch(explode(',', $this->getUserMeta(SAUCAL_API_SETTINGS_KEY)));
                $this->updateCache($newData);
                return $newData;
            } catch (HttpbinException $exception) {
                error_log($exception->getMessage());
                return $this->getUserMeta(self::CACHED_INFO_KEY);
            }
        }
        return $this->getUserMeta(self::CACHED_INFO_KEY);
    }

    /**
     * If cache is timed out
     * @return bool
     */
    protected function isTimedOut() {
        $timeStamp = $this->getUserMeta(self::CACHED_TIMESTAMP_KEY);
        return (empty($timeStamp) || time() > $timeStamp);
    }

    /**
     * If settings are different than previous time
     * @return bool
     */
    protected function isDifferentSettings() {
        $settings = $this->getUserMeta(self::CACHED_INFO_SETTINGS_KEY);
        return (empty($settings) || $settings !== $this->currentSettings);
    }

    /**
     * Update timestamp, settings and data at the same time
     * @param $newData
     */
    protected function updateCache($newData) {
        $this->updateUserMeta(self::CACHED_TIMESTAMP_KEY, (time() + self::TIMEOUT));
        $this->updateUserMeta(self::CACHED_INFO_SETTINGS_KEY, $this->currentSettings);
        $this->updateUserMeta(self::CACHED_INFO_KEY, $newData);
    }

    /**
     * @param $key
     * @return mixed
     */
    protected function getUserMeta($key) {
        return get_user_meta($this->userId, $key, true);
    }

    /**
     * @param $key
     * @param $value
     * @return bool|int
     */
    protected function updateUserMeta($key, $value) {
        return update_user_meta($this->userId, $key, $value);
    }

}

