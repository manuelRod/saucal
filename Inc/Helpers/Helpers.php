<?php
/**
 * Helper class with functions to be used on the widget
 *
 * User: manu
 * Date: 11/06/2018
 * Time: 17:10
 */

namespace Saucal\Inc\Helpers;

class Helpers {

    /**
     * Function to render a given template
     *
     * @param string $path
     * @param string $fileName
     */
    static function render($path, $fileName) {
        ob_start();
        require ($path . $fileName . '.php');
        ob_end_flush();
    }

}
