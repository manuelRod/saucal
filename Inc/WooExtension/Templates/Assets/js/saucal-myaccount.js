jQuery( function( $ ) {
    $('#settings').tagsInput({
        'height':'50px',
        'width':'100%',
        'interactive':true,
        'defaultText':'Type me',
        'delimiter': [','],
        'removeWithBackspace' : true,
        'minChars' : 0,
        'maxChars' : 0, // if not provided there is no limit
        'placeholderColor' : '#666666'
    });
});
