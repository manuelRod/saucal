<?php
/**
 * Content Template
 *
 * User: manu
 * Date: 11/06/2018
 * Time: 15:28
 */

defined( 'ABSPATH' ) || exit;

/**
 * Hook: saucal_before_content_settings.
 */
do_action( 'saucal_before_content_settings' );

?>
    <h3><?= _('Content', 'saucal-test'); ?></h3>
    <?php the_widget('Saucal\Inc\Widgets\SaucalWidget'); ?>
<?
/**
 * Hook: saucal_after_content_settings.
 */
do_action( 'saucal_after_content_settings' );