<?php
/**
 * Template to get user settings
 *
 * User: manu
 * Date: 11/06/2018
 * Time: 15:28
 */

defined( 'ABSPATH' ) || exit;

/**
 * Hook: saucal_before_api_settings.
 */
do_action( 'saucal_before_api_settings' );
$settings = get_user_meta(get_current_user_id(), SAUCAL_API_SETTINGS_KEY, true);
?>
    <h3><?= _('Your Settings', 'saucal-test'); ?></h3>
    <form class="edit-saucal" action="" method="post">
        <input name="settings" id="settings" value="<?= esc_attr( $settings ); ?>" />
        <p class="saucal-p-form">
            <?php wp_nonce_field( 'save_saucal_settings', 'save-saucal-settings-nonce' ); ?>
            <button type="submit" class="woocommerce-Button button" name="save_saucal_settings" value="<?php esc_attr_e( 'Save settings', 'saucal-test' ); ?>"><?php esc_html_e( 'Save settings', 'saucal-test' ); ?></button>
            <input type="hidden" name="action" value="save_saucal_settings" />
        </p>
    </form>
<?
/**
 * Hook: saucal_after_api_settings.
 */
do_action( 'saucal_after_api_settings' );
