<?php
/**
 * Class to hook all woocommerce related filters
 * User: manu
 * Date: 11/06/2018
 * Time: 12:48
 */

namespace Saucal\Inc\WooExtension;
use Saucal\Inc\Helpers\Helpers;

/**
 * Class WooExtension
 * @package Saucal\Inc\WooExtension
 */
class WooExtension {

    function hookToWordpress() {
        add_filter( 'query_vars',                       [$this, 'addApiQueryVar'], 0 );
        add_filter( 'woocommerce_account_menu_items',   [$this, 'myAccountMenuTabs'], 0 );
        add_action( 'woocommerce_account_api_endpoint', [$this, 'apiNewTabContent'] );
        add_action( 'wp_enqueue_scripts',               [$this, 'enqueueAssets'] );
        add_action( 'template_redirect',                [$this, 'saveSaucalSettings'] );

    }

    /**
     * Register 'api' query var
     *
     * @param $vars
     * @return array
     */
    function addApiQueryVar( $vars ) {
        $vars[] = 'api';
        return $vars;
    }

    /**
     * Hook into My Account menu tabs
     *
     * @param $tabs
     * @return array
     */
    function myAccountMenuTabs( $tabs ) {
        $tabs['api'] = __( 'SAU/CAL', 'saucal-test' );
 	    return $tabs;
    }

    /**
     * Template to display into api tab
     */
    function apiNewTabContent() {
        Helpers::render(plugin_dir_path(__FILE__) . 'Templates' . DIRECTORY_SEPARATOR, 'settings');
        Helpers::render(plugin_dir_path(__FILE__) . 'Templates' . DIRECTORY_SEPARATOR, 'content');
    }

    /**
     * Enqueue al styling and js for my account
     */
    function enqueueAssets() {
        if (!is_account_page()) {
            return;
        }

        $assets = plugin_dir_url(__FILE__) . 'Templates' . DIRECTORY_SEPARATOR . 'Assets' . DIRECTORY_SEPARATOR;
        wp_enqueue_style('jquery-tag-input-css', $assets . 'css' . DIRECTORY_SEPARATOR . 'jquery.tagsinput.min.css');
        wp_enqueue_style('saucal-myaccount-css', $assets . 'css' . DIRECTORY_SEPARATOR . 'saucal-myaccount.css');
        wp_enqueue_script('jquery-tag-input-js',
            $assets . 'js' . DIRECTORY_SEPARATOR . 'jquery.tagsinput.min.js',
            ['jquery']
        );
        wp_enqueue_script('saucal-myaccount-js',
            $assets . 'js' . DIRECTORY_SEPARATOR . 'saucal-myaccount.js',
            ['jquery-tag-input-js']
        );
    }

    /**
     * Settings Savings
     */
    function saveSaucalSettings() {
        if ('POST' !== strtoupper( $_SERVER['REQUEST_METHOD'])) {
            return;
        }

        if (empty( $_POST['action'] ) || 'save_saucal_settings' !== $_POST['action']) {
            return;
        }

        $nonceValue = wc_get_var($_REQUEST['save-saucal-settings-nonce'], wc_get_var( $_REQUEST['_wpnonce'], '' ));
        if (!wp_verify_nonce($nonceValue, 'save_saucal_settings')) {
            return;
        }

        $userId = get_current_user_id();

        if ($userId <= 0) {
            return;
        }

        $settings = (!empty($_POST['settings'])) ? wc_clean( $_POST['settings'] ): '';
        update_user_meta($userId, SAUCAL_API_SETTINGS_KEY, $settings);
        wc_add_notice(__( 'SAU/CAL settings updated successfully.', 'saucal-test' ));
        return;
    }
    
}

